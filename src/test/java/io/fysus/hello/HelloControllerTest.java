package io.fysus.hello;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class HelloControllerTest {

    @Test
    void sayHelloTest() {
        String hi = new HelloController().sayHello();
        assertEquals("Hello", hi);
    }

    @Test
    void testSayHelloTest() {
        String name = "Fredy";
        String hi = new HelloController().sayHello(name);
        assertEquals("Hello " + name, hi);
    }
}
package io.fysus.hello;

import javax.websocket.server.PathParam;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {


    @GetMapping("/hello")
    String sayHello() {
        return "Hello";
    }

    @GetMapping("/hello/{name}")
    String sayHello(@PathVariable String name) {
        return "Hello " + name;
    }

}
